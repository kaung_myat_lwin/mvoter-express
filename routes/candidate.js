var express = require('express');
var axios = require('axios');
var knayi = require('knayi-myscript');
var myanmarNumbers = require('myanmar-numbers');
var utils = require('./utils.js');

var router = express.Router();

/* GET candidate page. */
router.get('/:candidateId', function(req, res, next) {
  axios.get('http://api.maepaysoh.org/candidate/' + req.params.candidateId, {
    params: {
      token: '6cf7aa96-b270-5ad0-84dc-f75aa0947dd7',
      _with: 'party'
    }
  })
    .then(function(candidate){
      var curr_id = req.params.candidateId;
      var candidateResponse = candidate.data;
      var name_conversion = knayi.fontConvert(candidateResponse.data.name, 'zawgyi'),
          party_conversion = knayi.fontConvert(candidateResponse.data.party.party_name, 'zawgyi');
      var localize_birthdate = myanmarNumbers(new Date(utils.getDateFromStamp(candidateResponse.data.birthdate)), 'my').replace(/\./g, '/');
      var localize_age = myanmarNumbers(utils.calculateBirth(candidateResponse.data.birthdate), 'my');
      var candidateData = {
        curr_id: curr_id,
        name: candidateResponse.data.name,
        age: localize_age,
        name_zawgyi: name_conversion,
        photo_url: candidateResponse.data.photo_url,
        party: candidateResponse.data.party,
        party_zawgyi: party_conversion,
        legislature: candidateResponse.data.legislature,
        constituency: candidateResponse.data.constituency,
        birthdate: localize_birthdate,
        education: candidateResponse.data.education,
        occupation: candidateResponse.data.occupation,
        ethnicity: candidateResponse.data.ethnicity,
        religion: candidateResponse.data.religion,
        mother: candidateResponse.data.mother,
        father: candidateResponse.data.father
      }
      console.log(candidateData)
      res.render('candidate', { candidate: candidateData });
    })
    .catch(function(error){
      throw error;
    });
});

module.exports = router;
