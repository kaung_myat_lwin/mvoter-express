var express = require('express');
var axios = require('axios');
var knayi = require('knayi-myscript');
var router = express.Router();

/* GET home page. */
router.get('/:faqId', function(req, res, next) {
  axios.get('http://api.maepaysoh.org/faq/' + req.params.faqId, {
    params: {
      token: '6cf7aa96-b270-5ad0-84dc-f75aa0947dd7'
    }
  })
    .then(function(faq){
      var faqResponse = faq.data;
      var curr_id = req.params.faqId;
      var question_zawgyi = knayi.fontConvert(faqResponse.data.question, 'zawgyi'),
          answer_zawgyi = knayi.fontConvert(faqResponse.data.answer, 'zawgyi');
      var faqData = {
        curr_id: curr_id,
        category: faqResponse.data.category,
        question: faqResponse.data.question,
        question_zawgyi: name_conversion,
        answer: faqResponse.data.answer,
        answer_zawgyi: answer_zawgyi,
        article_or_section: faqResponse.data.article_or_section,
        law_or_source: faqResponse.data.law_or_source
      }
      res.render('faq', { faq: faqData });
    })
    .catch(function(error){
      throw error;
    });
});

module.exports = router;
