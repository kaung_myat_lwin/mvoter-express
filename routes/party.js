var express = require('express');
var axios = require('axios');
var knayi = require('knayi-myscript');
var myanmarNumbers = require('myanmar-numbers');
var router = express.Router();

var utils = require('./utils.js');

/* GET users listing. */
router.get('/:partyId', function(req, res, next) {
  axios.get('http://api.maepaysoh.org/party/' + req.params.partyId, {
    params: {
      token: '6cf7aa96-b270-5ad0-84dc-f75aa0947dd7'
    }
  })
    .then(function(party){
      var curr_id = req.params.partyId;
      var partyResponse = party.data;
      var name_conversion = knayi.fontConvert(partyResponse.data.party_name, 'zawgyi');

      var localize_establishment_date = myanmarNumbers(new Date(utils.getDateFromStamp(partyResponse.data.establishment_date)), 'my').replace(/\./g, '/'),
          localize_establishment_approval_date = myanmarNumbers(new Date(utils.getDateFromStamp(partyResponse.data.establishment_approval_date)), 'my').replace(/\./g, '/'),
          localize_registration_application_date = myanmarNumbers(new Date(utils.getDateFromStamp(partyResponse.data.registration_application_date)), 'my').replace(/\./g, '/'),
          localize_registration_approval_date = myanmarNumbers(new Date(utils.getDateFromStamp(partyResponse.data.registration_approval_date)), 'my').replace(/\./g, '/');

      var partyData = {
        curr_id: curr_id,
        id: partyResponse.data.id,
        party_name: partyResponse.data.party_name,
        party_name_zawgyi: name_conversion,
        party_name_english: partyResponse.data.party_name_english,
        party_flag: partyResponse.data.party_flag,
        region: partyResponse.data.region,
        member_count: partyResponse.data.member_count,
        leadership: partyResponse.data.leadership,
        chairman: partyResponse.data.chairman,
        establishment_date: localize_establishment_date,
        establishment_approval_date: localize_registration_approval_date,
        registration_application_date: localize_registration_application_date,
        registration_approval_date: localize_registration_approval_date,
        headquarters: partyResponse.data.headquarters,
        policy: partyResponse.data.policy
      }
      console.log(partyData);
      res.render('party', { party: partyData });
    })
    .catch(function(error){
      throw error;
    });
});

module.exports = router;
