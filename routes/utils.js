module.exports = {
  getDateFromStamp: function(timestamp){
    var dateStamp = new Date(),
        months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    if (timestamp < 0) {
      dateStamp.setTime(timestamp * 1000);
    } else {
      dateStamp.setTime(timestamp);
      //add empty array cuz idk wtf it is skipping a month lmao
      months.unshift('');
    }
    var yyyy = dateStamp.getFullYear(),
      mm = dateStamp.getMonth(),
      dd = dateStamp.getDate();
    return months[mm] + '/' + dd + '/' + yyyy;
  },
  calculateBirth: function(timestamp){
    var dateStamp = new Date(),
        currDateStamp = new Date();
    dateStamp.setTime(timestamp * 1000);
    var birthYear = dateStamp.getFullYear(),
        currentYear = currDateStamp.getFullYear();
    return currentYear - birthYear;
  }
};
